app.service('appsFlyerService', function() {
    function createAppsFlyerEvent(eventName, eventStatus, eventAnswer) {
        let eventName = eventName;
        let eventValues = {
            status: eventStatus,
            resposta: eventAnswer
        }
        window.plugins.appsFlyer.trackEvent(eventName, eventValues);
    }
});
