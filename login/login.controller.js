angular.
module('login')
.controller('loginCtrl', ['$scope', '$stateParams', '$ionicPopup', '$state', '$rootScope', '$ionicLoading', 'loginService', 'appsFlyerService',

    function($scope, $stateParams, $ionicPopup, $state, $rootScope, $ionicLoading) {

        document.addEventListener('deviceready', DeviceReady, false);

        function DeviceReady() {
            appsFlyerService.createAppsFlyerEvent('entrar', 'ok', '')
        }

        $scope.mascarar = function() {
            var valor_login = $('#emaillogar').val();
            if ($.isNumeric(valor_login)) {
                if(valor_login.length == 14) {
                    $('#emaillogar').mask('00.000.000/0000-00');
                } else {
                    $('#emaillogar').mask('000.000.000-00');
                }
            }
        }

        $scope.desmascarar = function() {
            $('#emaillogar').unmask();
        }

        $scope.registrar = function() {
            $state.go('registrar');
        }

        $scope.esqueceu = function() {
            $state.go('esqueceusuasenha');
        }  

        $scope.meajuda = function() {
            appsFlyerService.createAppsFlyerEvent('entrar_meajuda_email', 'ok', '');

            window.plugins.socialsharing.shareViaEmail(
                null, 
                null,
                'meajuda@appdindin.com', 
                null, 
                null, 
                null
            )
        }

        $scope.login = function() {
            if ($('#emaillogar').val() == '') {              
                appsFlyerService.createAppsFlyerEvent('entrar_login_email', '', 'email_vazio')

                $ionicPopup.alert({
                    title: 'Alerta',
                    template: 'Preencha o seu e-mail, CPF, CNPJ ou username'
                });
                return false;
            }
            if ($('#senhalogar').val() == '') {
                appsFlyerService.createAppsFlyerEvent('entrar_login_email', '', 'senha_vazia')
                
                $ionicPopup.alert({
                    title: 'Alerta',
                    template: 'Preencha a sua senha'
                });
                return false;
            }

            let data = {
                email: $('#emaillogar').val(),
                senha: $('#senhalogar').val()
            }

            $ionicLoading.show();
            loginService.login(data).then(res => {
                $ionicLoading.hide();
                loginService.saveLoggedUser(res.data.id);
                appsFlyerService.createAppsFlyerEvent('entrar_login_email', '', 'sucesso');
                $rootScope.atualizaPerfil();
                $state.go('telaprincipal');
            }, err => {
                $ionicLoading.hide();
                appsFlyerService.createAppsFlyerEvent('entrar_login_email', '', 'invalido');
                var alertPopup = $ionicPopup.alert({
                    title: 'Atenção',
                    template: data.mensagem
                });
            }); 
        }
    }
])
