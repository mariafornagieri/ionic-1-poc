app.service('loginService', function(API, $http) {
    function saveLoggedUser(id) {
        window.localStorage.setItem('dindin.usuario_id', id);
            angular.forEach(data.data, function(value, key) {
                window.localStorage.setItem('dindin.' + key, value);
                $rootScope[key] = value;
        });
    }

    function login() {
        let req =  {
            url: API + 'api/login',
            type: 'POST',
            headers: {
                'Authorization' : 'Basic ZGluZGluX3JlbmF0bzpkaW5kaW4='
            },
        }
        return $http(req);
    }
})